package com.springboot.springbootmybatisannotation.controller;

import com.springboot.springbootmybatisannotation.entity.address.result.UserResultVO;
import com.springboot.springbootmybatisannotation.mapper.UserMapper;
import com.springboot.springbootmybatisannotation.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @Version: 1.0
 * @Desc:
 */
@RestController
@Slf4j
public class UserController {

    @Autowired
    UserMapper userMapper;

    @GetMapping("/getUsers")
    public List<User> getUsers( ) {
        return userMapper.getAll();
    }

    @GetMapping("/getUser/{id}")
    public User getUser(@PathVariable("id") String id) {
        return userMapper.getUser(id);
    }

    @PostMapping("/add")
    public Long save(@RequestBody User user) {
        user.setCreateDate(new Date());
        return userMapper.insertUser(user);
    }

    @PostMapping("/update")
    public Long update(@RequestBody User user) {
        return userMapper.updateUser(user);
    }

    @DeleteMapping("/delete/{id}")
    public Long delete(@PathVariable("id") String id) {
        return userMapper.deleteUser(id);
    }

    /**
     * 根据多租户进行获得指定城市的数据
     *
     * @param cityId
     * @return
     */
    @GetMapping("/getUsersByCityId")
    public List<User> getUsersByCityId(@RequestParam(value = "cityId") Long cityId ) {
        return userMapper.getAll();
    }

    @PostMapping("/getUserAddressList")
    public  List<UserResultVO> getUserAddressList(){
        return  userMapper.getUserAddressList();
    }


}
