package com.springboot.springbootmybatisannotation.entity.address.result;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;

import java.util.Date;


@Data
@ToString
public class UserResultVO {

    /**
     * 用户主键id
     */
    private Integer id;

    /**
     * 用户的年龄
     */
    private Integer age;

    /**
     * 注册时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date date;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 默认收货地址
     */
    private String address;



}
