

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user_address
-- ----------------------------
DROP TABLE IF EXISTS `user_address`;
CREATE TABLE `user_address`  (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` int(5) NULL DEFAULT NULL COMMENT 'user 表的主键id',
  `addres_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户绑定的收货地址' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_address
-- ----------------------------
INSERT INTO `user_address` VALUES (1, 1, '成都市高新区复城国际');
INSERT INTO `user_address` VALUES (2, 2, '北京小米大厦');
INSERT INTO `user_address` VALUES (3, 3, '深圳松山湖华为总部');

SET FOREIGN_KEY_CHECKS = 1;
